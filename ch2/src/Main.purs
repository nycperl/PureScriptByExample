{-
 | 1. (Easy) Use the Math.pi constant to write a function circleArea 
 |    which computes the area of a circle with a given radius. 
 |    Test your function using PSCi (Hint: don’t forget to import pi by modifying the import Math statement).
-}

module Main where

import Prelude
import Math (pi)
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (CONSOLE, logShow)

circleArea :: Number -> Number
circleArea r = r * r * pi

main :: forall eff. Eff (console :: CONSOLE | eff) Unit
main = logShow( circleArea 1.0)
  
{-
 | output:
 | $ pulp run
 | * Building project in C:\Users\jshen\learnPS\PureScriptByExample\ch2
 | * Build successful.
 | 3.141592653589793
-}

{-
 | 2. (Medium) Use bower install to install the purescript-globals package as a dependency. 
 | Test out its functions in PSCi (Hint: you can use the :browse command in PSCi to browse the contents of a module).
 |
$ bower install purescript-globals --save
$ pulp psci
PSCi, version 0.11.4
Type :? for help

import Prelude
> :browse Global


decodeURI :: String -> String

decodeURIComponent :: String -> String

encodeURI :: String -> String

encodeURIComponent :: String -> String

infinity :: Number

isFinite :: Number -> Boolean

isNaN :: Number -> Boolean

nan :: Number

readFloat :: String -> Number

readInt :: Int -> String -> Number

>

-}

