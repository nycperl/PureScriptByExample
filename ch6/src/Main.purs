module Main where

import Prelude
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (CONSOLE, logShow)
-- import Data.Monoid
import Data.Foldable (class Foldable, foldMap, foldl, foldr)

{- up to ch6.3
| (Easy) Use the showShape function from the previous chapter to define a Show instance for the Shape type.
-}


data Shape
  = Circle Point Number
  | Rectangle Point Number Number
  | Line Point Point
  | Text Point String

data Point = Point
  { x :: Number
  , y :: Number
  }

origin :: Point
origin = Point { x, y }
       where
	x = 10.0
	y = 10.0

exampleCircle :: Shape
exampleCircle = Circle x y
	      where
		x :: Point
		x = origin

		y :: Number
		y = 10.0

instance showShape :: Show Shape where
	show (Circle c r) = "Circle"
	show (Rectangle c w h) = "Rectangle"
	show (Line p1 p2) = "Line"
	show (Text l t) = "Text"



--main :: forall eff. Eff (console :: CONSOLE | eff) Unit
--main = logShow( show exampleCircle )

{- 
$ pulp run
* Building project in C:\Users\jshen\learnPS\PureScriptByExample\ch6
* Build successful.
"Circle"
-}

-----------------------------------------

{- up to ch6.4
| Define Show and Eq instances for Complex.
-}

newtype Complex = Complex
   { real :: Number
   , imaginary :: Number
   }

instance showComplex :: Show Complex where
	show (Complex {real : r, imaginary : i}) = show(r) <> "+" <> show(i) <> "i"

complex :: Complex
complex = Complex { real : 3.0, imaginary : 1.0 }

-- main :: forall eff. Eff (console :: CONSOLE | eff) Unit
-- main = logShow( show complex )

{-
$ pulp run
* Building project in C:\Users\jshen\learnPS\PureScriptByExample\ch6
* Build successful.
"3.0+1.0i"
-}

instance eqComplex :: Eq Complex where
	eq ( Complex a ) (Complex b ) = a.real == b.real && a.imaginary == b.imaginary

complex2 :: Complex
complex2 = Complex { real : 3.0, imaginary : 2.0 }

-- main :: forall eff. Eff (console :: CONSOLE | eff) Unit
-- main = logShow( complex == complex2 )

{-
$ pulp run
* Building project in C:\Users\jshen\learnPS\PureScriptByExample\ch6
Compiling Main
* Build successful.
false
-}

-----------------------------------------

{- up to ch6.7
| 1. Write an Eq instance for the type NonEmpty a which reuses 
| the instances for Eq a and Eq (Array a).
-}

data NonEmpty a = NonEmpty a (Array a)
instance eqNonEmpty :: Eq a => Eq (NonEmpty a) where
	eq (NonEmpty x xs) (NonEmpty y ys) = x == y && xs == ys
non_empty_1 :: NonEmpty Int
non_empty_1 = NonEmpty 1 [2,3]
non_empty_2 :: NonEmpty Int
non_empty_2 = NonEmpty 1 [2,3]

-- main :: forall eff. Eff (console :: CONSOLE | eff) Unit
-- main = logShow( non_empty_1 == non_empty_2 )

{- output
$ pulp run
* Building project in C:\Users\jshen\learnPS\PureScriptByExample\ch6
Compiling Main
* Build successful.
true
-}

{- 2. (Medium) Write a Semigroup instance for NonEmpty a 
| by reusing the Semigroup instance for Array.
-}

instance showNonEmpty :: (Show a) => Show (NonEmpty a) where
	show (NonEmpty x xs) = show ([x] <> xs)

instance semigroupNonEmpty :: (Semigroup a) => Semigroup (NonEmpty a) where
	append (NonEmpty x xs) (NonEmpty y ys) = NonEmpty x (xs <> [y] <> ys)


{- 3. (Medium) Write a Functor instance for NonEmpty.
-}

instance functorNonEmpty :: Functor NonEmpty where
	map f (NonEmpty x xs) = NonEmpty (f x) (f <$> xs)


{- 4. Write an Ord instance for Extended a which reuses the Ord instance for a.
-}

data Extended a = Finite a | Infinite

instance eqExtended :: (Eq a) => Eq (Extended a) where
	eq (Finite x) (Finite y) = x == y
	eq (Finite _) Infinite   = false
  	eq Infinite (Finite _)   = false
	eq Infinite Infinite     = false

instance ordExtended :: (Ord a) => Ord (Extended a) where
	compare (Finite x) (Finite y) = compare x y
	compare (Finite _) Infinite = LT
	compare Infinite (Finite _) = GT
	compare Infinite Infinite = EQ	

{- 5. Write a Foldable instance for NonEmpty. 
| Hint: reuse the Foldable instance for arrays.
-}

instance foldableNonEmpty :: Foldable NonEmpty where
	foldr f b (NonEmpty x xs) = f x (foldr f b xs) 
	foldl f b (NonEmpty x xs) = f (foldl f b xs) x
	foldMap f (NonEmpty x xs) = (f x) <> (foldMap f xs)

{- 6. Write a Foldable instance for OneMore f:
-}

data OneMore f a = OneMore a (f a)
instance foldableOneMore :: (Foldable f) => Foldable (OneMore f) where
    foldr f b (OneMore x xs) = f x (foldr f b xs)
    foldl f b (OneMore x xs) = f (foldl f b xs) x
    foldMap f (OneMore x xs) = (f x) <> (foldMap f xs)

main :: forall eff. Eff (console :: CONSOLE | eff) Unit
main = logShow( "ok" )

